###############################################################################
## Christine Plumejeaud-Perreau, U.M.R 7301 MIGRINTER
## 07 octobre 2020
## Script d'analyse des données INSEE pour IMHANA
## Mise à jour 21 sept 2023
###############################################################################

meslibrairiesR <- "C:/Tools/R/R4"
# Ajouter meslibrairiesR dans ces chemins : 
.libPaths(c( .libPaths(), meslibrairiesR) )


install.packages("tidyverse", meslibrairiesR)
install.packages("mapsf", meslibrairiesR)
install.packages("dplyr", meslibrairiesR)

library(tidyverse)
library(mapsf)
library(dplyr)  

setwd("C:/Travail/MIGRINTER/Labo/IMHANA/micro-projets/")
getwd() 

## Sauver / reprendre son travail sous R
# https://mtes-mct.github.io/parcours-r/m1/sauvegarder-son-travail.html 
dir.create("./outputs")

save(list = ls(), file = "outputs/env_entier.RData") 
# sauvegarde de tout l'environnement sur le répertoire choisi 

rm(list = ls()) # suppression de notre environnement dans R 

load("outputs/env_entier.RData") 
# chargement de l'environnement stocké sur l'ordinateur 


######################################################################### programme de lecture du fichier détail de l'Enquête emploi 2019_fichier CSV.R


data = read.csv2("FD_RP_1968-2018.csv", header = TRUE, sep = ";", quote = "\"",
                dec = ".", fill = TRUE, comment.char = "")	

# Exemple de calcul d'indicateurs

# Au préalable : installer le package dplyr
# https://cran.r-project.org/web/packages/dplyr/index.html
library(dplyr)  ;

# Total de pop en 2018 /1968
summarize(data,tous = sum(POND*(  AN_RECENS %in% c(1968)  )))
summarize(data,tous = sum(POND*(  AN_RECENS %in% c(1968)  & REG_RES_20 %in% c(75) )))

# Total de pop immigrée en 2018/1968: Etranger né Etranger
summarize(data,immi = sum(POND*( !(NATIO %in% c('000', '001')) & AN_RECENS %in% c(1968) & REG_NAIS %in% c(99) )))
summarize(data,immi75 = sum(POND*( !(NATIO %in% c('000', '001')) & AN_RECENS %in% c(1968) & REG_NAIS %in% c(99) & REG_RES_20 %in% c(75) )))

#Total de pop étrangere en 2018
summarize(data,etrangers = sum(POND*( !(NATIO %in% c('000', '001')) & AN_RECENS %in% c(1968) )))
summarize(data,etrangers75 = sum(POND*( !(NATIO %in% c('000', '001')) & AN_RECENS %in% c(1968) & REG_RES_20 %in% c(75) )))

#né à l'Etranger : REG_NAIS %in% c(99)

# 000 Français de naissance
# 001 Français par acquisition
# 1IT Italiens
# 1ES Espagnols
# 1PT Portugais
# 2**   Autres nationalités d'Europe

#424 771 047
# 16 958 880
# immi en 2018 :  6 490 646


######################################################################### programme de lecture du fichier détail de l'Enquête emploi 2019_fichier CSV.R


library(tidyverse)
library(lubridate)
data <- data %>%
  mutate(annee_date = as.Date(as.character(AN_RECENS), format = "%Y")) %>% 
  mutate(annee_int = as.numeric(AN_RECENS)) %>% 
  mutate_at(vars(NATIO, AN_RECENS, REG_NAIS, REG_RES_20, DEP_TRA_20, TYP_ACT, SEXE, DIPL, STAT_CONJ, REG_TRA_20, NES4, DEP_RES_20, DEP_NAIS, DEP_TRA_20 ), list(as.factor))  %>%
  mutate_at(vars(POND, AGE_REV), list(as.numeric)) 

# class(data$AN_RECENS)
# mutate_at(vars(AN_RECENS), list(ymd))
# data <- data %>%
#     mutate(data, annee = year(ymd(AN_RECENS))) 
# 
# annee = year(ymd(head(data$AN_RECENS)))
# head(data$AN_RECENS)
# ymd(as.character(c(1968)))
# year(ymd("1968-01-01"))
# as.numeric(head(data$AN_RECENS))
# class(head(data$AN_RECENS))
# as.Date(as.character("1968"),     format = "%Y")
# as.Date(as.character(unique(data$AN_RECENS)), format = "%Y")
# 
# data <- data %>% mutate_at(vars(REG_RES_20), list(as.factor))

#Construire un résumé par année par nationalite pour tous ceux nés à l'étranger

resume <- data %>% filter (REG_NAIS %in% c(99)) %>%
  group_by(AN_RECENS, NATIO) %>%
  summarise(total = round(sum(POND)), 
            n = n()
            )

#Construire un résumé par année par nationalite pour tous les étrangers / immigrés 
#France

resume <- data %>% filter (!(NATIO %in% c('000', '001')) & REG_NAIS %in% c(99)) %>%
  group_by(AN_RECENS, NATIO) %>%
  summarise(total = round(sum(POND))
  ) %>%
  arrange(desc(total))

## 21 sept : garder les francais par acquisition 001 nés à l'étranger
resume <- data %>% filter (!(NATIO %in% c('000')) & REG_NAIS %in% c(99)) %>%
  group_by(AN_RECENS, NATIO, DEP_NAIS, REG_RES_20, DEP_TRA_20, TYP_ACT, SEXE, DIPL, STAT_CONJ, NES4) %>%
  summarise(total = round(sum(POND))
  ) %>%
  arrange(REG_RES_20,DEP_TRA_20,AN_RECENS,NATIO)
write.table(resume, "./outputs/cpp_resume_FD_RP_1968-2018_001inclus.csv", sep = ";", dec=".", row.names=FALSE, fileEncoding = "UTF-8")

## 21 sept : pour comparer avec francais quelque soit leur lieu de naissance
resume <- data %>% filter ((NATIO %in% c('000')) ) %>%
  group_by(AN_RECENS, REG_RES_20, DEP_TRA_20, TYP_ACT, SEXE, DIPL, STAT_CONJ, NES4) %>%
  summarise(total = round(sum(POND))
  ) %>%
  arrange(REG_RES_20,DEP_TRA_20,AN_RECENS)
write.table(resume, "./outputs/cpp_resume_FD_RP_1968-2018_000francais.csv", sep = ";", dec=".", row.names=FALSE, fileEncoding = "UTF-8")


#Résumé par année et par nationalité pour tous les étrangers / immigrés  vivant en Nouvelle-Aquitaine

resume <- data %>% filter (!(NATIO %in% c('000', '001')) & REG_NAIS %in% c(99) & REG_RES_20 %in% c(75) ) %>%
  group_by(AN_RECENS, NATIO) %>%
  summarise(total = round(sum(POND))
  ) %>%
  arrange(desc(total))
  
#Chiffres de la France entière en 2018
resume[resume$AN_RECENS==2018, ]

resume[resume$AN_RECENS==1968, ]

# Prendre la variable Total (qui correspond à la pondération, sinon, plantage)

#Construire un résumé par année AN_RECENS, par nationalité NATIO et  par région de résidence REG_RES_20
resume <- data %>% filter (REG_NAIS %in% c(99)) %>%
  group_by(AN_RECENS, NATIO, REG_RES_20) %>%
  summarise(total = round(sum(POND)))


#Chiffres de la Nouvelle-Aquitaine en 1968
resume[resume$REG_RES_20==75 & resume$AN_RECENS==1968, ]

write.table(resume, "./outputs/resume_FD_RP_1968-2018.csv", sep = "\t")
# A cartographier dans le maillage région 2020. Fourni aux étudiants
write.table(resume, "./outputs/cpp_resume_FD_RP_1968-2018.csv", sep = ";", dec=".", row.names=FALSE, fileEncoding = "UTF-8")



#Construire un résumé par année AN_RECENS, par nationalité NATIO et  par région de résidence REG_RES_20

#La modalité "01 : Guadeloupe" comprend les territoires de Saint-Martin et de Saint-Barthélemy avant 2009.
# 01 = 01+07+08 avant 2009
#Pour les Recensements de 1968 et 1975, les Comores et Mayotte sont présents dans la modalité 0X. 
# A partir de 1982, cette modalité est éclatée en 06 pour Mayotte. 
#Les îles autonomes des Comores sont compris dans la modalité 99.
#99 = 06+0x+99 avant 1982

# 01 / 971 Guadeloupe
# 02 / 972 Martinique
# 03/ 973 Guyane
# 04/ 974 Réunion
# 05 / 975 Saint-Pierre-et-Miquelon
# 06 / 976 Mayotte
# 07 / 977 Saint-Barthélemy
# 08 / 978 Saint-Martin
# 09 Territoire français des Afars et Issas (Djibouti)
# 0X / 97X Comores y.c Mayotte, Terres australes
# 9Y / 9Y Pacifique (Wallis-et-Futuna, Polynésie Française, Nouvelle- Calédonie, Ile de Clipperton)
# 99 Etranger / et autre COM (Territoire français des Afars et Issas (Djibouti))

#, 01, 02, 03, 04, 07, 08, 06, 0x
resume <- data %>% filter (REG_NAIS %in% c(99, 01, 02, 03, 04, 07, 08, 06, '0x')) %>%
  group_by(DEP_NAIS, AN_RECENS, NATIO, REG_RES_20, DEP_TRA_20, TYP_ACT, SEXE, DIPL, STAT_CONJ, NES4) %>%
  summarise(total = round(sum(POND)))

#Chiffres de la Nouvelle-Aquitaine en 1968
resume[resume$REG_RES_20==75 & resume$AN_RECENS==1968, ]

write.table(resume, "./outputs/resumeComplet_FD_RP_1968-2018.csv", sep = "\t")
# A cartographier dans le maillage région 2020. Fourni aux étudiants
write.table(resume, "./outputs/cpp_resumeComplet_FD_RP_1968-2018.csv", sep = ";", dec=".", row.names=FALSE, fileEncoding = "UTF-8")


# NES4
# Secteur d'activité(en 4 postes)
# 1 Agriculture
# 2 Industrie
# 3 BTP
# 4 Tertiaire
# 9 Inactifs ou chômeurs

##################################################################################
## Lire un fichier SPSS
# 
library(foreign)
#dataframe <- read.spss("C:/Travail/MIGRINTER/Labo/IMHANA/data/lil-0610.sav/SPSS/TD_ACT1_2006.sav", to.data.frame = TRUE) 

dataframe <- read.spss("C:/Travail/MIGRINTER/Labo/IMHANA/data/lil-0610.2006/SPSS/TD_ACT1_2006.sav", to.data.frame = TRUE)
dataframe


summary(dataframe)

## la base NAT3_2006

# SEXE : Sexe
# 1 : Hommes
# 2 : Femmes
# INATC : Indicateur de nationalité (Français/Etranger)
# 1 : Français
# 2 : Etrangers
# CS1_8 : Catégorie socioprofessionnelle regroupée (8 postes)
# 1 : Agriculteurs exploitants
# 2 : Artisans, commerçants, chefs entreprise
# 3 : Cadres et professions intellectuelles supérieures
# 4 : Professions intermédiaires
# 5 : Employés
# 6 : Ouvriers
# 7 : Retraités
# 8 : Autres personnes sans activité professionnelle

remove(dataframe)
dataframe <- read.spss("C:/Travail/CNRS_poitiers/MIGRINTER/Labo/IHMANA/data/lil-0610.sav/SPSS/TD_NAT3A_2006.sav", to.data.frame = TRUE) 



summary(dataframe)

En format Texte, chaque ligne comporte les informations relatives aux champs suivants, séparées par le caractère ' ;' :
  - niveau de la zone géographique (NIVEAU) : commune (COM) ou arrondissement municipal (ARM)
- code de la zone (CODGEO)
- pour chaque variable utilisée, selon l'ordre des variables indiqué ci-dessus, code de la modalité concernée par le
croisement de critères
- effectif (NB) associé à la zone géographique et au croisement de critères indiqués dans les champs précédents.


## IMG1_2006

dataframe <- read.spss("C:/Travail/MIGRINTER/Labo/IMHANA/data/lil-0610.2006/SPSS/TD_IMG1_2006.sav", to.data.frame = TRUE) 
dataframe <- read.spss("C:/Travail/MIGRINTER/Labo/IMHANA/data/lil-0610.2006/SPSS/td_mig1_2006.sav", to.data.frame = TRUE) 
#INATC2_SEXE1_AGEMEN855_IRANR1
dataframe <- read.spss("C:/Travail/MIGRINTER/Labo/IMHANA/data/lil-0610.2006/SPSS/TD_MIG2_2006.sav", to.data.frame = TRUE) 


labels <- attr(dataframe, "variable.labels")
labels
#CS2_2482_IRANR4

# SEXE : Sexe
# 1 : Hommes
# 2 : Femmes
# AGE4 : Âge regroupé (4 classes d'âges)
# 00 : Moins de 15 ans
# 15 : 15 à 24 ans
# 25 : 25 à 54 ans
# 55 : 55 ans ou plus
# TACTR : Type d'activité
#                      11 : Actifs ayant un emploi
#                      12 : Chômeurs
#                      21 : Retraités ou préretraités
#                      22 : Elèves, étudiants, stagiaires non rémunérés
#                      24 : Femmes ou hommes au foyer
#                      26 : Autres inactifs
#                      IMMI : Situation quant à l'immigration
# 1 : Immigrés
# 2 : Non immigrés

summary(dataframe)


dataframe <- read.spss("C:/Travail/MIGRINTER/Labo/IMHANA/data/lil-0610.sav/SPSS/TD_POP1B_2006.sav", to.data.frame = TRUE) 
v <- dataframe[dataframe$C0 == '86165',]$C2
labels <- attr(dataframe, "variable.labels")

#######################
dataframe <- read.spss("C:/Travail/MIGRINTER/Labo/IMHANA/data/lil-0610.2006/SPSS/TD_POP1A_2006.sav", to.data.frame = TRUE) 
v <- dataframe[dataframe$C0 == '86165',]

#Toute la population en 2006
sum(as.vector(unlist(v[2:length(v)])))
#parfait (cumsum)

## Traitement des noms des colonnes
labels <- attr(dataframe, "variable.labels")
labels <- data.frame(labels) 
formatlabels <- labels %>% 
  separate(col=labels, into=c("code", "etiquette"), sep=':')
formatlabels[2, 1] <- "C1"
formatlabels[2, 2] <- "AGEPYR1000_SEXE1"

library(dplyr)
library(data.table)
library(stringr)

#Tous les garçons
test <- formatlabels %>%
      filter(str_detect(etiquette,"SEXE1"))

# Classer suivant nos besoins [0-25[ / [25-65[ / 65 et plus
classe <- c("commune", "jeunes", "jeunes", "jeunes", "jeunes", "jeunes", "jeunes", "jeunes", "jeunes","jeunes", "jeunes", "adultes", "adultes", "adultes", "adultes", "adultes", "adultes", "vieux", "vieux", "vieux", "vieux")
formatlabels <- cbind(formatlabels, classe)

#
test <- formatlabels %>%
  filter(etiquette %like% "%SEXE1%") # filtrer sr les garcons

number <- which(formatlabels$classe == "jeunes") 



## 
sum(as.vector(unlist(v[number])))

################################################################################
##
## Le 26 décembre 2023 : il manquait les CSP, et je retreins l'export à la NA 
##
################################################################################

## 26 dec. : garder les francais par acquisition 001 nés à l'étranger
resume <- data %>% filter (!(NATIO %in% c('000')) & REG_NAIS %in% c(99)& REG_RES_20 %in% c(75)) %>%
  group_by(AN_RECENS, NATIO, DEP_NAIS, DEP_TRA_20, TYP_ACT, SEXE, DIPL, STAT_CONJ, NES4, CSP) %>%
  summarise(total = round(sum(POND))
  ) %>%
  arrange(DEP_TRA_20,AN_RECENS,NATIO)
write.table(resume, "./outputs/cpp_resume_FD_RP_1968-2018_001inclus_CSP.26.12.2023.csv", sep = ";", dec=".", row.names=FALSE, fileEncoding = "UTF-8")

## 26 dec. : pour comparer avec francais quelque soit leur lieu de naissance
resume <- data %>% filter ((NATIO %in% c('000')) & REG_RES_20 %in% c(75)) %>%
  group_by(AN_RECENS, DEP_TRA_20, TYP_ACT, SEXE, DIPL, STAT_CONJ, NES4, CSP) %>%
  summarise(total = round(sum(POND))
  ) %>%
  arrange(DEP_TRA_20,AN_RECENS)
write.table(resume, "./outputs/cpp_resume_FD_RP_1968-2018_000francais_CSP.26.12.2023.csv", sep = ";", dec=".", row.names=FALSE, fileEncoding = "UTF-8")

###############################################################################
## le 19 mars 2024
#############################################################################

dataframe <- read.spss("C:/Travail/MIGRINTER/Labo/IMHANA/data/lil-0610.2006/SPSS/td_mig1_2006.sav", to.data.frame = TRUE)

dataframe <- read.spss("C:/Travail/MIGRINTER/Labo/IMHANA/data/lil-0610.2006/SPSS/TD_MIG2_2006.sav", to.data.frame = TRUE) 
labels <- attr(dataframe, "variable.labels")
labels <- data.frame(labels) 

formatlabels <- labels %>% 
  separate(col=labels, into=c("code", "etiquette"), sep=':')

#Tous les garçons
test <- formatlabels %>%
  filter(str_detect(etiquette,"SEXE1"))
