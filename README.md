# IMHANA Project


Set of jupyter notebooks, R and SQL scripts produced by Christine Plumejeaud and the team, allowing for versioning code in the IMHANA project.

This project is funded by Nouvelle-Aquitaine Region (https://imhana.hypotheses.org) - [Licence AGPL v3](https://opensource.org/licenses/AGPL-3.0)

UMR. MIGRINTER, GEOLAB, ITEM, PASSAGE are some of the research units involved in. 



[Learn more about creating GitLab projects.](https://docs.gitlab.com/ee/gitlab-basics/create-project.html)
