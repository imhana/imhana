---------------------------------------------------------------------
-- Script pour faire une carto de la Nouvelle-Aquitaine par cantons
-- Géographie socio-économique : migration, logement, emploi, niveau de revenu, maillage administratif
-- Auteur : Christine Plumejeaud, UMR 7301 MIGRINTER
-- Création 26 janvier 2024
-- C:\Travail\MIGRINTER\Labo\IMHANA\data\Data_canton-ville\imhana_cantons.sql
---------------------------------------------------------------------


CREATE extension postgis;

-- commune 2022
C:\Tools\OSgeo4W\bin\ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=imhana password=postgres schemas=cantons" C:\Travail\MIGRINTER\Labo\IMHANA\data\Data_canton-ville\geo\ADMIN-EXPRESS_3-1__SHP_LAMB93_FXX_2022-12-20\ADMIN-EXPRESS\1_DONNEES_LIVRAISON_2022-12-20\ADE_3-1_SHP_LAMB93_FXX\COMMUNE.shp -a_srs EPSG:2154 -nln commune2022 -nlt MULTIPOLYGON

-- commune carto (avec statut) 2022
C:\Tools\OSgeo4W\bin\ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=imhana password=postgres schemas=cantons" C:\Travail\MIGRINTER\Labo\IMHANA\data\Data_canton-ville\geo\ADMIN-EXPRESS-COG-CARTO_3-1__SHP_LAMB93_FXX_2022-04-15\ADMIN-EXPRESS-COG-CARTO\1_DONNEES_LIVRAISON_2022-04-15\ADECOGC_3-1_SHP_LAMB93_FXX\COMMUNE.shp -a_srs EPSG:2154 -nln communeCarto2022 -nlt MULTIPOLYGON

-- epci carto  2022
C:\Tools\OSgeo4W\bin\ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=imhana password=postgres schemas=cantons" C:\Travail\MIGRINTER\Labo\IMHANA\data\Data_canton-ville\geo\ADMIN-EXPRESS-COG-CARTO_3-1__SHP_LAMB93_FXX_2022-04-15\ADMIN-EXPRESS-COG-CARTO\1_DONNEES_LIVRAISON_2022-04-15\ADECOGC_3-1_SHP_LAMB93_FXX\EPCI.shp -a_srs EPSG:2154 -nln epciCarto2022 -nlt MULTIPOLYGON

-- dep carto  2022
C:\Tools\OSgeo4W\bin\ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=imhana password=postgres schemas=cantons" C:\Travail\MIGRINTER\Labo\IMHANA\data\Data_canton-ville\geo\ADMIN-EXPRESS-COG-CARTO_3-1__SHP_LAMB93_FXX_2022-04-15\ADMIN-EXPRESS-COG-CARTO\1_DONNEES_LIVRAISON_2022-04-15\ADECOGC_3-1_SHP_LAMB93_FXX\DEPARTEMENT.shp -a_srs EPSG:2154 -nln departementCarto2022 -nlt MULTIPOLYGON

-- region carto  2022
C:\Tools\OSgeo4W\bin\ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=imhana password=postgres schemas=cantons" C:\Travail\MIGRINTER\Labo\IMHANA\data\Data_canton-ville\geo\ADMIN-EXPRESS-COG-CARTO_3-1__SHP_LAMB93_FXX_2022-04-15\ADMIN-EXPRESS-COG-CARTO\1_DONNEES_LIVRAISON_2022-04-15\ADECOGC_3-1_SHP_LAMB93_FXX\REGION.shp -a_srs EPSG:2154 -nln regionCarto2022 -nlt MULTIPOLYGON


-- communes de NA
select * from cantons.communecarto2022 c
where c.insee_reg='75';

create table cantons.communesNA22 as 
(select * from cantons.communecarto2022 c
where c.insee_reg='75');

alter table cantons.communesNA22 add column surface_km2 float;
update cantons.communesNA22  set surface_km2 = st_area(wkb_geometry)/1000000;

drop table cantons.cantonsNA22;

create table cantons.cantonsNA22 as 
(select c.insee_reg , c.insee_dep ,  c.insee_can, c.insee_dep||c.insee_can as CV, sum(surface_km2) as surface_km2, st_union(wkb_geometry) as geom  
from cantons.communesNA22 c  where c.insee_reg='75'
group by c.insee_reg , c.insee_dep , c.insee_can 
);

-- 255 (au lieu des 299 si group by c.insee_arr)
select * from cantons.cantonsNA22 where CV = '7901'; --2 si on agrège par arrondissement, attention supprimer c.insee_arr dans le group_by

select c.insee_reg , c.insee_dep ,  c.insee_can, c.insee_dep||c.insee_can as CV, sum(surface_km2) as surface_km2 
-- st_union(wkb_geometry) as geom  
from cantons.communesNA22 c  where c.insee_reg='75'
group by c.insee_reg , c.insee_dep ,  c.insee_can ;
-- 255

create table cantons.epciNA22 as 
(select c.siren_epci , array_agg(distinct c.insee_can) as list_cantons, sum(surface_km2) as surface_km2, st_union(wkb_geometry) as geom  
from cantons.communesNA22 c  where c.insee_reg='75'
group by c.siren_epci
);
-- 156

-- joindre epciNA22 avec epciCarto2022
select code_siren, nom, nature from cantons.epciCarto2022;

alter table cantons.epciNA22 add nom text;
alter table cantons.epciNA22 add nature text;

update cantons.epciNA22 e set nom = ec.nom, nature=ec.nature
from cantons.epciCarto2022 ec where ec.code_siren=e.siren_epci;

---------------------------------------------------------
-- joindre communesNA22 avec epciCarto2022

alter table cantons.communesNA22 add nomEPCI text;
alter table cantons.communesNA22 add natureEPCI text;

update cantons.communesNA22 e set nomEPCI = ec.nom, natureEPCI=ec.nature
from cantons.epciCarto2022 ec where ec.code_siren=e.siren_epci;

alter table cantons.communesNA22 add zone_etude text;
update cantons.communesNA22 set zone_etude = '01_Bressuire' where nomEPCI = 'CA du Bocage Bressuirais'; 
--33
update cantons.communesNA22 set zone_etude = '02_Mille-Vaches' where nomEPCI in ('CC Haute-Corrèze Communauté', 'CC des Portes de Vassivière', 'CC Vézère-Monédières-Millesources', 'CC Creuse Sud Ouest','CC Creuse Grand Sud');
-- 171
update cantons.communesNA22 set zone_etude = '03_Périgord-Vert' where nomEPCI in ('CC du Périgord Nontronnais', 'CC Périgord-Limousin', 'CC La Rochefoucauld - Porte du Périgord');
-- 77
update cantons.communesNA22 set zone_etude = '04_Pays-Foyen' where nomEPCI in ('CC du Pays Foyen');
-- 20


select zone_etude, nomEPCI , c.siren_epci , c.insee_dep , c.insee_com , c.nom , c.population , c.surface_km2 
from cantons.communesNA22 c
where c.zone_etude is not null 
order by zone_etude, nomEPCI, nom

select zone_etude, nomEPCI , c.siren_epci , sum(c.population) , sum(c.surface_km2)
from cantons.communesNA22 c
where c.zone_etude is not null 
group by zone_etude, nomEPCI , c.siren_epci
order by zone_etude, nomEPCI

alter table cantons.communesNA22 add column visite_date date;
alter table cantons.communesNA22 add column visite_ordre int;
alter table cantons.communesNA22 add column visite_questionnaire int;

update cantons.communesNA22 c set visite_date = '2024-06-17', visite_ordre = 1, visite_questionnaire=13
where c.nom = 'Excideuil' and c.insee_com  = '24164';

update cantons.communesNA22 c set visite_date = '2024-06-18', visite_ordre = 2, visite_questionnaire=4
where c.nom = 'La Coquille' and c.insee_com  = '24133';

update cantons.communesNA22 c set visite_date = '2024-06-18', visite_ordre = 3, visite_questionnaire=10
where c.nom = 'Thiviers' and c.insee_com  = '24551';

update cantons.communesNA22 c set visite_date = '2024-06-19', visite_ordre = 4, visite_questionnaire=5
where c.nom = 'Piégut-Pluviers' and c.insee_com  = '24328';
-----------------------------------------
-- utiliser les données de l'ADISP, communes 2006
select count(*) from  communes.communes_2006 c ; -- 36726
select count(*) , substring("CODGEO" for 2) 
from  communes.communes_2006 c 
where substring("CODGEO" for 2) in ('16', '17', '19', '23', '24', '33', '40', '47', '64', '79', '86', '87')
group by substring("CODGEO" for 2) ;
-- 36726
select count(*) 
from  communes.communes_2006 c 
where substring("CODGEO" for 2) in ('16', '17', '19', '23', '24', '33', '40', '47', '64', '79', '86', '87')
-- 4505 en Nouvelle-Aquitaine en 2006

select insee_reg , count(*) from  communecarto2023 c 
group by insee_reg;
-- 4308 en Nouvelle-Aquitaine en 2023

select insee_reg ,insee_dep, count(*) 
from  communecarto2023 c 
where insee_reg = '75'
group by insee_reg, insee_dep
order by insee_reg, insee_dep;
-- 4308 en Nouvelle-Aquitaine

select index, "CODGEO", "POP2.C336", "YEAR" 
from communes.communes_2006 c , communecarto2023 c2 
where c."CODGEO" = c2.insee_com;

--
-- intégrer la couche géographique correspondante de 2006 : geofla 2008  ?
SET PGCLIENTENCODING=LATIN1

C:\Tools\OSgeo4W\bin\ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=imhana password=postgres schemas=communes" C:\Travail\MIGRINTER\Labo\IMHANA\data\data_ADISP_communes_SPSS\geo\GEOFLA_1-1_TOUSTHEMES_SHP_LAMB93_FXX_2008-01-01\GEOFLA\1_DONNEES_LIVRAISON_2022-05-00156\GEOFLA_1-1_SHP_LAMB93_FR-ED081\COMMUNES\COMMUNE.shp -a_srs EPSG:2154 -nln commune2008 -nlt MULTIPOLYGON

select distinct nom_region from communes.commune2008 c ; --22
select count(*) from communes.commune2008 c 
where code_dept in ('16', '17', '19', '23', '24', '33', '40', '47', '64', '79', '86', '87');
--22
-- 4505 en Nouvelle-Aquitaine en 2008 : parfait pour faire la carte

-- attribuer les epci2022 à ces anciennes communes
alter table communes.commune2008 rename to geocom2008;
alter table communes.geocom2008 add column siren_epci2022 text;
alter table communes.geocom2008 add column nom_epci2022 text;
alter table communes.geocom2008 add column ogc_fid_epci2022 int;

select e.code_siren, e.nom, c.insee_com, c.nom_comm , c.code_dept , c.nom_dept , st_centroid(c.wkb_geometry)
from cantons.epcicarto2022 e , communes.geocom2008 c
where 
-- st_contains(e.wkb_geometry, st_setsrid(st_makepoint(c.x_chf_lieu, c.y_chf_lieu), 2154))
 code_dept in ('16', '17', '19', '23', '24', '33', '40', '47', '64', '79', '86', '87');
-- 1566

select  c.insee_com, c.nom_comm , c.code_dept , c.nom_dept , 
c.x_chf_lieu, c.y_chf_lieu, st_centroid(c.wkb_geometry) 
-- st_transform(st_setsrid(st_makepoint(c.x_chf_lieu, c.y_chf_lieu), 27572) , 2154)
from cantons.epcicarto2022 e, communes.geocom2008 c
where 
-- st_contains(e.wkb_geometry, st_setsrid(st_makepoint(c.x_chf_lieu, c.y_chf_lieu), 2154))
st_contains(e.wkb_geometry, st_centroid(c.wkb_geometry))
and code_dept in ('16', '17', '19', '23', '24', '33', '40', '47', '64', '79', '86', '87');
-- 4499 : il en manque 6

--------------------------------------------------------
-- correction le 29 MARS
update communes.geocom2008 c set siren_epci2022 = null;--36588
-- commencer par 
update communes.geocom2008 c set siren_epci2022 = e.siren_epci  
from cantons.communecarto2022  e
where e.insee_com  = c.insee_com ;--34814
-- et ensuite : 
update communes.geocom2008 c set siren_epci2022 = e.code_siren , nom_epci2022=e.nom , ogc_fid_epci2022 =e.ogc_fid 
from cantons.epcicarto2022 e
where siren_epci2022 is null and st_contains(e.wkb_geometry, st_centroid(c.wkb_geometry));
-- 1774

-- select 34814 + 1774
-- 36588
--------------------------------------------------------

update communes.geocom2008 c set nom_epci2022=e.nom , ogc_fid_epci2022 =e.ogc_fid 
from cantons.epcicarto2022 e
where e.code_siren  = c.siren_epci2022 ;-- 36454

select * from communes.geocom2008 c where siren_epci2022 is null;

-----------------------
update communes.geocom2008 c set siren_epci2022 = e.code_siren , nom_epci2022=e.nom , ogc_fid_epci2022 =e.ogc_fid 
from cantons.epcicarto2022 e
where st_contains(e.wkb_geometry, st_centroid(c.wkb_geometry))
and code_dept in ('16', '17', '19', '23', '24', '33', '40', '47', '64', '79', '86', '87');
-- faux pour commune 40120 par exemple

select siren_epci2022, * from communes.geocom2008 g where g.insee_com = '40120';

select array_agg(nom_comm), array_agg(insee_com)  
from communes.geocom2008 
where code_dept in ('16', '17', '19', '23', '24', '33', '40', '47', '64', '79', '86', '87') 
and siren_epci2022 is null;
--{ILE-D'AIX,BAYON-SUR-GIRONDE,BLAYE,GAURIAC,PLASSAC,SAINT-ANDRONY}

select c.nom_m , c.siren_epci , c.nomepci  from cantons.communesna22 c where nom_m in ('ILE-D''AIX','BAYON-SUR-GIRONDE','BLAYE','GAURIAC','PLASSAC','SAINT-ANDRONY')
--GAURIAC	200023794	CC de Blaye
--SAINT-ANDRONY	243300811	CC de l'Estuaire
--BLAYE	200023794	CC de Blaye
--BAYON-SUR-GIRONDE	200023794	CC de Blaye
--PLASSAC	200023794	CC de Blaye
--ILE-D'AIX	200041762	CA Rochefort Océan

select  c.insee_com, c.nom_comm , c.code_dept , c.nom_dept , e.nom ,
st_area(st_intersection(e.wkb_geometry, c.wkb_geometry))/st_area(c.wkb_geometry) , 
st_area(st_intersection(e.wkb_geometry, c.wkb_geometry))
from cantons.epcicarto2022 e, communes.geocom2008 c
where siren_epci2022 is null and
st_intersects(e.wkb_geometry, c.wkb_geometry)
and code_dept in ('16', '17', '19', '23', '24', '33', '40', '47', '64', '79', '86', '87')
and st_area(st_intersection(e.wkb_geometry, c.wkb_geometry))/st_area(c.wkb_geometry) > 0.01;

update communes.geocom2008 c set siren_epci2022 = e.code_siren , nom_epci2022=e.nom , ogc_fid_epci2022 =e.ogc_fid 
from cantons.epcicarto2022 e
where siren_epci2022 is null and st_intersects(e.wkb_geometry, c.wkb_geometry)
and code_dept in ('16', '17', '19', '23', '24', '33', '40', '47', '64', '79', '86', '87')
and st_area(st_intersection(e.wkb_geometry, c.wkb_geometry))/st_area(c.wkb_geometry) > 0.01;


--- reste de la France

update communes.geocom2008 c set siren_epci2022 = e.code_siren , nom_epci2022=e.nom , ogc_fid_epci2022 =e.ogc_fid 
from cantons.epcicarto2022 e
where st_contains(e.wkb_geometry, st_centroid(c.wkb_geometry));
--and code_dept in ('16', '17', '19', '23', '24', '33', '40', '47', '64', '79', '86', '87');
-- 36575


update communes.geocom2008 c set siren_epci2022 = e.code_siren , nom_epci2022=e.nom , ogc_fid_epci2022 =e.ogc_fid 
from cantons.epcicarto2022 e
where siren_epci2022 is null and st_intersects(e.wkb_geometry, c.wkb_geometry)
-- and code_dept in ('16', '17', '19', '23', '24', '33', '40', '47', '64', '79', '86', '87')
and st_area(st_intersection(e.wkb_geometry, c.wkb_geometry))/st_area(c.wkb_geometry) > 0.01;
-- 3

select array_agg(nom_comm), array_agg(insee_com)  
from communes.geocom2008 
where siren_epci2022 is null;
-- 4 communes n'appartiennent à aucune intercom : les iles bretonnes, Reg = 53, et vendéennes Reg = 52
-- {OUESSANT,L'ILE-D'YEU,ILE-DE-BREHAT,ILE-DE-SEIN}
-- L'ILE-D'YEU : NR, Reg = 52
-- ILE-DE-SEIN : NR, Reg = 53
-- OUESSANT : NR, Reg = 53
-- ILE-DE-BREHAT : NR, Reg = 53

-------------------------------------------------------------------
-- rajouts de quelques champs pour pouvoir faire les résumés statistiques en fonction de ces critères, facilement. 
-- 
-- Note : la somme de tout ceux qui ont un département renseignés = la France métropolitaine
alter table communes.communes_2006 add column insee_reg2022 text; -- on ne renseigne que la région Nouvelle-Aquitaine, les autres sont à NULL
alter table communes.communes_2006 add column insee_dep2022 text;-- on ne renseigne que le départements de métropole (les dom en 97xxxx ne sont pas dans les géométries)
alter table communes.communes_2006 add column siren_epci2022 text; -- pour faire le lien avec la géometrie EPCI et pour les résumés
alter table communes.communes_2006 add column zoneetude_epci2022 text; -- pour faire le le lien avec la géometrie des zones d'étude et pour les résumés
alter table communes.communes_2006 add column ogc_fid_geo2008 int; -- pour faire le lien avec la géometrie communale

update communes.communes_2006 c set ogc_fid_geo2008 = g.ogc_fid 
from communes.geocom2008 g
where c."CODGEO" = g.insee_com ;
-- 36586 

select index, "CODGEO" , ogc_fid_geo2008 from communes.communes_2006 where ogc_fid_geo2008 is null;
select * from communes.geocom2008 g where nom_comm = 'LYON';
-- 6912	6912	123	69123	LYON
select * from communes.geocom2008 g where nom_comm = 'MARSEILLE';
-- 1540	1540	055	13055

select count(*) from  communes.geocom2008 c ; -- 36588 à la place de 36726 (data INSEE)

select * from communes.geocom2008 g where g.insee_com  = '97101';
35317 -- ??
62847  -- ??
75056  -- ??
97101, etc. -- les 112 communes des dom-tom

update communes.communes_2006 c set ogc_fid_geo2008 = 6912 where ogc_fid_geo2008 is null and "CODGEO" like '69%';
-- 9 arrondissements de LYON
update communes.communes_2006 c set ogc_fid_geo2008 = 1540 where ogc_fid_geo2008 is null and "CODGEO" like '13%';
-- 16 arrondissements de MARSEILLE
update communes.communes_2006 c set insee_dep2022 = g.code_dept , siren_epci2022 = g.siren_epci2022 
from communes.geocom2008 g
where c.ogc_fid_geo2008 = g.ogc_fid ;
-- 36611 à la place de 36726 (data INSEE)

-- correction
update communes.communes_2006  c set insee_dep2022 = g.code_dept , siren_epci2022 = g.siren_epci2022 
from communes.geocom2008 g
where c.ogc_fid_geo2008 = g.ogc_fid ;
-- 36611, OK
update communes.communes_zones2020  c set insee_dep2022 = g.code_dept , siren_epci2022 = g.siren_epci2022 
from communes.geocom2008 g
where c.ogc_fid_geo2008 = g.ogc_fid ;
-- 36611, OK


update communes.communes_2006 c set insee_reg2022 = '75' where insee_dep2022 in ('16', '17', '19', '23', '24', '33', '40', '47', '64', '79', '86', '87');
-- 4505

update communes.communes_2006 c set zoneetude_epci2022 = '01_Bressuire' where siren_epci2022 = '200040244'; 
--45, OK
update communes.communes_2006 c set zoneetude_epci2022 = '02_Mille-Vaches' where siren_epci2022 in ('200067189', '248719353', '200066645', '200066744', '200044014');
-- 173, OK
update communes.communes_2006 c set zoneetude_epci2022 = '03_Périgord-Vert' where siren_epci2022 in ('200071819', '242400752', '242401024');
-- 80, OK
update communes.communes_2006 c set zoneetude_epci2022 = '04_Pays-Foyen' where siren_epci2022 in ('243301371');
-- 19, OK

update communes.communes_2006 c set insee_reg2022 = c2.insee_reg 
from cantons.communecarto2022 c2
where c."CODGEO" = c2.insee_com and insee_reg2022 is null;
-- insee_dep2022 in ('16', '17', '19', '23', '24', '33', '40', '47', '64', '79', '86', '87');
-- 30508, OK


update communes.communes_2006 c set insee_reg2022 = c2.insee_reg 
from communes.geocom2008 g, cantons.communecarto2022 c2
where insee_reg2022 is null 
and c.ogc_fid_geo2008 = g.ogc_fid and st_contains(c2.wkb_geometry, st_centroid(g.wkb_geometry) )
-- 1601, OK
-- ok

update communes.communes_2006 c set insee_reg2022 = substring("CODGEO" for 3),  insee_dep2022 = substring("CODGEO" for 3)
where insee_reg2022 is null and  substring("CODGEO" for 2) = '97';
-- 112, OK
-- ok


select "CODGEO", "YEAR", insee_reg2022, insee_dep2022, siren_epci2022,  zoneetude_epci2022, ogc_fid_geo2008 
from communes.communes_2006 where insee_reg2022 is null;
--1713
-- 0

create table communes.communes_zones2020 as 
(select "CODGEO", "YEAR", insee_reg2022, insee_dep2022, siren_epci2022,  zoneetude_epci2022, ogc_fid_geo2008
from communes.communes_2006  );
-- 36726

/*
select case when (IMG1_C1+IMG1_C2)=0 then null else IMG1_C1/ (IMG1_C1+IMG1_C2) end
from (
select siren_epci2022, sum("IMG1.C1") as IMG1_C1 , sum("IMG1.C2") as IMG1_C2
--, sum("IMG1.C1")/(sum("IMG1.C1")+sum("IMG1.C2"))*100
from communes.communes_2006
where insee_reg2022 = '75' --zoneetude_epci2022 is not null
group by siren_epci2022) as k;
*/
/**/

SELECT pg_catalog.pg_class.relname 
FROM pg_catalog.pg_class JOIN pg_catalog.pg_namespace ON pg_catalog.pg_namespace.oid = pg_catalog.pg_class.relnamespace 
WHERE pg_catalog.pg_class.relname = 'data_2006_lot1' AND pg_catalog.pg_class.relkind = ANY (ARRAY['r', 'p', 'f', 'v', 'm']) AND pg_catalog.pg_namespace.nspname = 'communes';

select "CODGEO", "POP2_C1" + "POP2_C2" from data_2006;

alter table data_2006 rename to data_2006_lot1;
alter table data_2006_lot1 drop column  "index";
alter table data_2006_lot1 drop column  "YEAR";


update metadata_2006 set "table"='data_2006_lot2' where "table" = 'data_2006_lot1';--553
update metadata_2006 set "table"='data_2006_lot1' where "table" = 'data_2006_lot0';--959

----------------------------------------------------------
-- a refaire après le script python qui sauve metadata_2006
alter table data_2006_lot1 add constraint pk primary key   ("CODGEO") ;
-- todo
delete from metadata_2006 where code_var = 'CODGEO';
-- 12 todo
--  TYPLR1_CS1_81_STOCD10
alter table metadata_2006 add column nbdim int;
alter table metadata_2006 add column dim01 varchar(24);
alter table metadata_2006 add column label01 text;
alter table metadata_2006 add column dim02 varchar(24);
alter table metadata_2006 add column label02 text;
alter table metadata_2006 add column dim03 varchar(24);
alter table metadata_2006 add column label03 text;
alter table metadata_2006 add column dim04 varchar(24);
alter table metadata_2006 add column label04 text;
alter table metadata_2006 add column dim05 varchar(24);
alter table metadata_2006 add column label05 text;

-- update metadata_2006 set nbdim = where fichier = '';

-- update metadata_2006 set dim01 = 'AGE400', label01= 'Moins de 15 ans' where fichier = 'TD_IMG1_2006' and  position ( 'AGE400' IN "Combinaison"  ) > 0 ;

-- select * from metadata_2006 m where m.label01 = '' or m.label02 = '' or m.label03 = '' or m.label04 = '' or m.label05 = '';
-- ensuite un code python complete la table, pour les champs nbdim et dim01, label01 , dim02, label02, dim03, label03, dim04, label04, dim05, label05 
select racine_fichier ,  code_var , colonne,  "Combinaison", 
nbdim, dim01, label01 , dim02, label02, dim03, label03, dim04, label04, dim05, label05    
from metadata_2006 m;
-- Export pour faire le fichier Excel correspondant
----------------------------------------------------------

select distinct siren_epci2022 , zoneetude_epci2022, insee_reg2022  from communes.communes_zones2020 
where siren_epci2022  in (select code_siren from cantons.epcicarto2022 e);

-- 28 epci à cheval sur des régions
select  siren_epci2022
from communes.communes_zones2020 
group by siren_epci2022
having count(distinct insee_reg2022) > 1;

drop table communes.epci_zones2020
create table communes.epci_zones2020 as
(select  siren_epci2022 , zoneetude_epci2022, insee_reg2022, insee_dep2022, count("CODGEO") as nbcommunes  
from communes.communes_zones2020 
group by siren_epci2022 , zoneetude_epci2022, insee_reg2022, insee_dep2022
); --1351

select  siren_epci2022
from communes.epci_zones2020 
group by siren_epci2022
having count(distinct insee_reg2022) > 1;
--26 à cheval sur des régions

200030435
200035129
200035723
200040277
200040681
200066462
200067668
200068088
200068765
200069300
200069722
200070308
200070332
200071033
200071140
200071884
200072106
200072676
243500741
244400610
246100663
246401756
247600588
247800550
248200016
248400251
248400335

select  siren_epci2022
from communes.epci_zones2020 
group by siren_epci2022
having count(distinct insee_dep2022) > 1;
-- 95 à cheval sur des départements

200054781/200057990
200054781/200058014

200006682
200023620
200030435
200030526
200034023
200034700
200035129
200035723
200040111
200040277
200040491
200040590
200040624
200040681
200041887
200054781
200054807

alter table communes.epci_zones2020 add column ok boolean;
update communes.epci_zones2020 set ok = null;
update communes.epci_zones2020 set ok = false where  siren_epci2022  in (
select  siren_epci2022
from communes.epci_zones2020 
group by siren_epci2022
having count(distinct insee_dep2022) > 1);
-- 194

update communes.epci_zones2020 z  set ok = true
from (
	select siren_epci2022, max(nbcommunes) as kmax
	from communes.epci_zones2020
	where siren_epci2022 in (
		select  siren_epci2022
		from communes.epci_zones2020 
		group by siren_epci2022
		having count(distinct insee_dep2022) > 1
	)
	group by siren_epci2022
) as k
where z.siren_epci2022 = k.siren_epci2022 and z.nbcommunes = k.kmax;
--95

select * from communes.epci_zones2020 z where ok is true;
-- 200066744
-- 243301371

select * from communes.epci_zones2020 z where siren_epci2022 = '200067106';
-- 40120

select * from communes.communes_zones2020  where siren_epci2022 = '243301371' -- Foyen
select * from communes.communes_zones2020  where siren_epci2022 = '200066744' -- Mille-Vaches
select * from communes.communes_zones2020  where siren_epci2022 = '200040681' -- a cheval 2 depts (26 maj et 84) et 2 régions (84 et 93)
-- 245900758
select * from communes.communes_zones2020  where siren_epci2022 = '245900758' -- a cheval 2 depts (26 maj et 84) et 2 régions (84 et 93)

select * from communes.epci_zones2020 z 
where siren_epci2022 = '245900758';
-- lequel supprimer : le département dont la surface de commune est la plus petite
select superficie, insee_com, nom_comm , code_dept  from geocom2008 g where g.siren_epci2022 = '245900758'

select sum(superficie),  code_dept  
from geocom2008 g 
where g.siren_epci2022 = '245900758' 
group by code_dept;
-- 6506	59
-- 6301	62

update communes.epci_zones2020 set ok = false where siren_epci2022 = '245900758' and insee_dep2022 = '62';

-- controle
select  siren_epci2022
from communes.epci_zones2020 
where ok is not false
group by siren_epci2022
		having count(distinct insee_dep2022) > 1
-- OK
		
select * from communes.epci_zones2020 z where siren_epci2022 is null order by insee_reg2022;
-- les communes des domtom ne sont pas dans des EPCI ?
select * from communes.communes_zones2020 where siren_epci2022 is null and insee_reg2022 not like '97%';
--35317	2006	53
--62847	2006	32
--75056	2006	11

select * from communes.geocom2008 g where g.insee_com in ('75056', '62847', '35317');
-- Trois communes absentes de la géographie 2008
-- (mais aussi de geo 2006 et de toute façon, l'insee indique bien que les données 2006 vont avec la géographie 2008)

select  siren_epci2022, zoneetude_epci2022, insee_dep2022 , insee_reg2022   from communes.epci_zones2020 where ok is not false and siren_epci2022 is not null;
-- 1244

select  siren_epci2022, zoneetude_epci2022, insee_dep2022 , insee_reg2022   
from communes.epci_zones2020 where ok is not false and siren_epci2022 is not null and zoneetude_epci2022 is not null
order by zoneetude_epci2022, insee_dep2022;
-- 200040244	01_Bressuire	79	75
-- 200066645	02_Mille-Vaches	19	75
-- 200066645	02_Mille-Vaches	19	75
-- 248719353	02_Mille-Vaches	87	75
-- 242400752	03_Périgord-Vert	24	75
-- 243301371	04_Pays-Foyen	33	75

-- composition

select "CODGEO", siren_epci2022, zoneetude_epci2022, insee_dep2022 , insee_reg2022   from communes.communes_zones2020 c, communes.epci_zones2020 e

----------------------------------------------------------------------------------------------------------------
-- Import des données 2023 pour la carto
-- https://www.insee.fr/fr/information/2383182
-- les fiches chiffres détaillés et le dossier complet proposent des résultats pour les recensements 2020, 2014 et 2009. 
-- La géographie des résultats 2009, 2014 et 2020 est celle en vigueur au 1er janvier 2023

--  PGCLIENTENCODING=LATIN1
-- fichiers dans C:\Travail\MIGRINTER\Labo\IMHANA\data\data_ADISP_communes_SPSS\geo\

-- commune carto (avec statut) 2023
C:\Tools\OSgeo4W\bin\ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=imhana password=postgres schemas=cantons" C:\Travail\MIGRINTER\Labo\IMHANA\data\data_ADISP_communes_SPSS\geo\ADMIN-EXPRESS-COG-CARTO_3-1__SHP_LAMB93_FXX_2023-05-03\ADMIN-EXPRESS-COG-CARTO\1_DONNEES_LIVRAISON_2023-05-03\ADECOGC_3-1_SHP_LAMB93_FXX\COMMUNE.shp -a_srs EPSG:2154 -nln communeCarto2023 -nlt MULTIPOLYGON

-- epci carto  2023
C:\Tools\OSgeo4W\bin\ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=imhana password=postgres schemas=cantons" C:\Travail\MIGRINTER\Labo\IMHANA\data\data_ADISP_communes_SPSS\geo\ADMIN-EXPRESS-COG-CARTO_3-1__SHP_LAMB93_FXX_2023-05-03\ADMIN-EXPRESS-COG-CARTO\1_DONNEES_LIVRAISON_2023-05-03\ADECOGC_3-1_SHP_LAMB93_FXX\EPCI.shp -a_srs EPSG:2154 -nln epciCarto2023 -nlt MULTIPOLYGON

select count(*) from cantons.communecarto2022 c 
-- 34826
select count(*) from cantons.communecarto2023 c 
-- 34816
-- 10 communes de moins en 2023 qu'en 2022.

select count(*) from cantons.epcicarto2022 e  
-- 1243
select count(*) from cantons.epcicarto2023 e ; 
-- 1243
-- même nombre d'EPCI

select code_siren, nom, nature from cantons.epcicarto2023 e
EXCEPT
(select code_siren, nom, nature from cantons.epcicarto2022 e
);
-- 58 ont changé : mais je ne vois pas la différence
select e.code_siren, e.nom||'|', e.nature||'|', e3.code_siren, e3.nom||'|', e3.nature||'|'
from cantons.epcicarto2023 e3, cantons.epcicarto2023 e
where (e3.code_siren = e.code_siren or e3.nom like e.nom) and e.code_siren in ( '200017846', '200071934', '200068914');

select nom, insee_com ,  siren_epci  from cantons.communecarto2023 e
EXCEPT
(select nom, insee_com ,  siren_epci from cantons.communecarto2022 e
);
-- 31 changts de communes
 
-- Terval	85289	248500415
select nom, insee_com ,  siren_epci  from cantons.communecarto2023 e
where e.insee_com = '85289';
select nom, insee_com ,  siren_epci  from cantons.communecarto2022 e
where e.insee_com = '85289';
-- changt de nom

select e.nom, e.insee_com ,  e.siren_epci,   c.nom, c.insee_com ,  c.siren_epci
from cantons.communecarto2022 e, cantons.communecarto2023 c
where e.insee_com = c.insee_com and e.insee_com = '85289';


select  insee_com   from cantons.communecarto2023 e
EXCEPT
(select insee_com  from cantons.communecarto2022 e
);
-- 27676 : 1 commune nouvelle

select  insee_com   from cantons.communecarto2022 e
EXCEPT
(select insee_com  from cantons.communecarto2023 e
);
-- 11 communes disparues
/*
71492
16140
27058
51063
85037
50015
51637
09255
85053
01039
02077
*/

-----------------------------------------------------------------------
-- import des données 2017 pour traiter les données 2015
-----------------------------------------------------------------------

C:\Tools\OSgeo4W\bin\ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=imhana password=postgres schemas=cantons" C:\Travail\MIGRINTER\Labo\IMHANA\data\data_ADISP_communes_SPSS\geo\ADMIN-EXPRESS-COG_1-0__SHP__FRA_2017-06-19\ADMIN-EXPRESS-COG\1_DONNEES_LIVRAISON_2017-06-19\ADE-COG_1-0_SHP_LAMB93_FR\COMMUNE.shp -a_srs EPSG:2154 -nln communeCarto2017 -nlt MULTIPOLYGON

-- C:\Tools\OSgeo4W\bin\ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=imhana password=postgres schemas=cantons" C:\Travail\MIGRINTER\Labo\IMHANA\data\data_ADISP_communes_SPSS\geo\ADMIN-EXPRESS-COG_1-0__SHP__FRA_2017-06-19\ADMIN-EXPRESS-COG\1_DONNEES_LIVRAISON_2017-06-19\ADE-COG_1-0_SHP_LAMB93_FR\EPCI.shp -a_srs EPSG:2154 -nln communeEpci2017 -nlt MULTIPOLYGON
-- alter table cantons.communeepci2017 rename to epcicarto2017
select distinct type_epci from cantons.epcicarto2017 c 
/*
ME
CU
CA
CC
*/
select  type_epci, c.nom_epci, c.code_epci from cantons.epcicarto2017 c
where c.code_epci in ('200040244', '200067189', '248719353', '200066645', '200066744', '200044014', '200071819', '242400752', '242401024', '243301371')

-- import des codes des communes des données 2015 via python

alter table communes_zones2015  add column  insee_reg2022 varchar(3);
alter table communes_zones2015  add column  insee_dep2022 varchar(3);
alter table communes_zones2015  add column  siren_epci2022 varchar(10);
alter table communes_zones2015  add column  ogc_fid_geo2017 int;
alter table communes_zones2015  add column  zoneetude_epci2022 text;

alter table communes_zones2015  alter column  siren_epci2022 type text;

--------------------------
-- 1. associer communes_zones2015 et communecarto2017
--------------------------
select count(*) from communes_zones2015;--35396

select count(c.ogc_fid) from cantons.communecarto2017 c;-- 35287

select distinct substring("COMMUNE" from 1 for 2), "REGION"  
from communes_zones2015 c 
where c."COMMUNE" not in  (select c.insee_com from cantons.communecarto2017 c)
and c."COMMUNE" not in  (select '0'||c.insee_com from cantons.communecarto2017 c)
order by "REGION" ;

select  "COMMUNE", "REGION"  
from communes_zones2015 c 
where c."COMMUNE" not in  (select c.insee_com from cantons.communecarto2017 c)
and '0'||c."COMMUNE" not in  (select c.insee_com from cantons.communecarto2017 c)
order by "REGION" ;
-- OK seuelement les 112 DOM-TOM

update communes_zones2015 z set ogc_fid_geo2017 = c.ogc_fid 
from cantons.communecarto2017 c
where z."COMMUNE"  = c.insee_com;
-- 32105
update communes_zones2015 z set ogc_fid_geo2017 = c.ogc_fid 
from cantons.communecarto2017 c
where ogc_fid_geo2017 is null and '0'||z."COMMUNE"  = c.insee_com;
-- 3179
select 112 + 32105 + 3179 -- 35396

--------------------------
-- 2. est-ce que les EPCI 2017 et EPCI 2022 correspondent ?
--------------------------

-- en 2017, le code EPCI est code_epci
select * from cantons.communecarto2017 c
where c.code_epci not in (select siren_epci from cantons.communecarto2022);
-- 675 code_epci modifiés ou sans correspondances

alter table cantons.communecarto2017 add column siren_epci2022 text;
update cantons.communecarto2017 set siren_epci2022 = null;

update cantons.communecarto2017 c17 set siren_epci2022 = c22.siren_epci
from cantons.communecarto2022 c22
where c17.insee_com = c22.insee_com ;
-- 34824

select * from cantons.communecarto2017 where siren_epci2022 is null; --463
-- 200072049 , LA PERUSE 16259
select * from cantons.communecarto2022 where insee_com = '16259'; -- disparue

update cantons.communecarto2017 c17 set siren_epci2022 = e.code_siren 
from cantons.epcicarto2022 e
where siren_epci2022 is null and st_contains(e.wkb_geometry, st_centroid(c17.wkb_geometry));
-- 462

-- Reste (à la main)
-- 241700640 SAINT-ROMAIN-SUR-GIRONDE 17392
select * from cantons.communecarto2022 where insee_com = '17392'; -- disparue
-- cas particulier où le centroide tombe dans le fleuve, hors EPCI
update cantons.communecarto2017 c set siren_epci2022='241700640' where c.insee_com  = '17392';
-- 1

select * from cantons.communecarto2017 where siren_epci2022 is null; --0

select insee_com from communes.geocom2008 g  where nom_comm in ('OUESSANT','L''ILE-D''YEU','ILE-DE-BREHAT','ILE-DE-SEIN');
-- 29155,85113,22016,29083
-- ces communes ont disparue ?
select * from cantons.communecarto2017 g  where g.insee_com in ('29155','85113','22016','29083');
-- code_epci : ZZZZZZZZZ
select * from cantons.communecarto2017 g  where g.siren_epci2022 = 'NR';

select insee_com from communes.geocom2008 g  where nom_comm in ('OUESSANT','L''ILE-D''YEU','ILE-DE-BREHAT','ILE-DE-SEIN');

drop table cantons.epcicarto2017 ;

-- 35284
update communes_zones2015 z set siren_epci2022 = null;--35396
update communes_zones2015 z set siren_epci2022 = c.siren_epci2022 
from cantons.communecarto2017 c
where c.ogc_fid = z.ogc_fid_geo2017 ;


-- select distinct "REGION" from communes_zones2015 where siren_epci2022 is null; -- DOMTOM (1, 2, 3, 4)
--------------------------
-- zoneetude_epci2022
--------------------------
update communes.communes_zones2015 c set zoneetude_epci2022 = '01_Bressuire' where siren_epci2022 = '200040244'; 
--38, OK
update communes.communes_zones2015 c set zoneetude_epci2022 = '02_Mille-Vaches' where siren_epci2022 in ('200067189', '248719353', '200066645', '200066744', '200044014');
-- 175, OK
update communes.communes_zones2015 c set zoneetude_epci2022 = '03_Périgord-Vert' where siren_epci2022 in ('200071819', '242400752', '242401024');
-- 78, OK
update communes.communes_zones2015 c set zoneetude_epci2022 = '04_Pays-Foyen' where siren_epci2022 in ('243301371');
-- 20

--------------------------
-- Region et département 2022
--------------------------
update communes.communes_zones2015 c set insee_reg2022 = c22.insee_reg , insee_dep2022= c22.insee_dep
from cantons.communecarto2022 c22
where c."COMMUNE"  = c22.insee_com ;-- 31678

update communes.communes_zones2015 c set insee_reg2022 = c22.insee_reg , insee_dep2022= c22.insee_dep
from cantons.communecarto2022 c22
where insee_dep2022 is null and '0'||c."COMMUNE"  = c22.insee_com ;-- 3143


update communes.communes_zones2015 c set insee_reg2022 = c22.insee_reg , insee_dep2022= c22.insee_dep
from cantons.communecarto2022 c22, cantons.communecarto2017 g
where c.insee_dep2022 is null and c.ogc_fid_geo2017 = g.ogc_fid  
 and st_contains(c22.wkb_geometry, st_centroid(g.wkb_geometry)); --462

 -- Reste (à la main)
-- 241700640 SAINT-ROMAIN-SUR-GIRONDE 17392
select * from cantons.communecarto2022 where insee_com = '17392'; -- disparue
-- cas particulier où le centroide tombe dans le fleuve, hors EPCI
update communes.communes_zones2015 c set insee_reg2022='75', insee_dep2022='17' where c."COMMUNE"  = '17392';
-- 1

update communes.communes_zones2015 c set  insee_reg2022 = substring("COMMUNE" for 3),  insee_dep2022 = substring("COMMUNE" for 3)
where c.insee_dep2022  is null and c."COMMUNE" like '97%';--112


select * from communes.communes_zones2015 where insee_dep2022 is null; --112 des DOM-TOM

select 31678 + 3143
/*
i.	insee_reg2022 de communeCarto2022 par code insee égaux : 34821 cas
ii.	inclusion du centroïde de la commune 2017 dans la commune 2022, et récupération de insee_reg2022 de la communeCarto2022 : 462 cas
iii.	Cas particulier de SAINT-ROMAIN-SUR-GIRONDE (17392) qui a disparu en 2022, mais par sa forme, le centroïde de la commune est hors limites.
iv.	Trois premiers chiffres du CODGEO si DOMTOM (CODGEO ~ 97%) : 112 cas
*/

select * from communes.communes_zones2015 where "COMMUNE" like '97%'; --112 des DOM-TOM
select count(distinct "COMMUNE") from communes.communes_zones2015;

select "COMMUNE", siren_epci2022 from communes.communes_zones2015
where "COMMUNE" = '1006'	and "REGION" = 84	;

select * from communes_zones2020 cz where cz.zoneetude_epci2022 is not null;

select * from epci_zones2020 ez  where ez.zoneetude_epci2022 is not null;

select count(*), insee_dep2022 
from communes_zones2020 cz 
where cz.zoneetude_epci2022 = '02_Mille-Vaches'
group by cz.insee_dep2022;

---------------------------------------------------------------------------------
-- traitement des données Cagé-Piketty
-- annoncées en géographie 2022 mais pas vraiment en fait (10 communes de différence)
-- je garde Paris en entier (75056) 
---------------------------------------------------------------------------------
select count(*) from cantons.communecarto2022 c
where c.insee_reg 

-- import des données de Cagé-Piketty
create schema Cage_Piketty

select insee_com, nom_m, statut, population  from cantons.communecarto2022 c ;
-- 43076	CONNANGLES	Commune simple
select insee_com, nom_m, statut, population  from cantons.communecarto2022 c where insee_com = '2695';
select insee_com, nom_m, statut, population  from cantons.communecarto2022 c where nom_m = 'SAINT-THIBAUT';
select insee_com, nom_m, statut, population  from cantons.communecarto2022 c where nom_m like '%SAINT-THIBAUT%';
-- 02054	BAZOCHES-ET-SAINT-THIBAUT	Commune simple	542
-- 10419	VILLEMOYENNE	AUBE	GRAND EST
select insee_com, nom_m, statut, population  from cantons.communecarto2022 c where nom_m like '%VILLEMOYENNE%';
-- 10419	VILLEMOYENNE	Commune simple	753
select insee_com, nom_m, insee_dep, statut, population  from cantons.communecarto2022 c where nom_m like '%JARDIN%';

select codecommune, nomcommune , nomdep , nomreg  from cage_piketty.popcommunes p 
where '0'||codecommune not in (select trim(c.insee_com) from cantons.communecarto2022 c)
and codecommune not in (select trim(c.insee_com) from cantons.communecarto2022 c);
-- 2695	SAINT-THIBAUT	AISNE	HAUTS DE FRANCE



select insee_com, nom_m, statut, population  from cantons.communecarto2022 c where nom_m like '%FAUTE%';


-- les 10  cas particuliers : des fusions qui ont toutes eu lieu au 1er janvier 2022
85307	FAUTE-SUR-MER	VENDEE	PAYS DE LA LOIRE -- 85001
2695	SAINT-THIBAUT	AISNE	HAUTS DE FRANCE -- 02054
16010	AMBLEVILLE	CHARENTE	NOUVELLE AQUITAINE -- 16186
19092	JARDIN	CORREZE	NOUVELLE AQUITAINE --  Montaignac-Saint-Hippolyte (19143) qui devient Montaignac-sur-Doustre (commune nouvelle).
24089	CAZOULÈS	DORDOGNE	NOUVELLE AQUITAINE -- 24325
24314	ORLIAGUET	DORDOGNE	NOUVELLE AQUITAINE -- 24325
25134	CHÂTILLON-SUR-LISON	DOUBS	BOURGOGNE FRANCHE COMTE -- 25185
25628	VILLERS-SOUS-MONTROND	DOUBS	BOURGOGNE FRANCHE COMTE -- 25375
26219	MUREILS	DROME	AUVERGNE RHONE ALPES -- 26216
56049	CROIXANVEC	MORBIHAN	BRETAGNE -- 56213

-- Depuis le 1er janvier 2022, elle est une commune déléguée de la commune nouvelle de L'Aiguillon-la-Presqu'île1. 
-- Depuis le 1er janvier 2022, elle est une commune déléguée de la commune nouvelle de Lignières-Ambleville1. 
-- Depuis le 1er janvier 2022, elle est une commune déléguée de la commune nouvelle de Pechs-de-l'Espérance1. 
-- https://www.insee.fr/fr/metadonnees/cog/communes/COM19092-le-jardin
-- https://fr.wikipedia.org/wiki/Pechs-de-l%27Esp%C3%A9rance
-- https://fr.wikipedia.org/wiki/Cussey-sur-Lison
-- Depuis le 1er janvier 2022, elle a fusionné avec Mérey-sous-Montrond pour former la commune nouvelle des Monts-Ronds1. 
-- Depuis le 1er janvier 2022, elle est une commune déléguée de la commune nouvelle de Saint-Jean-de-Galaure1. 
-- Depuis le 1er janvier 2022, elle est une commune déléguée de la commune nouvelle de Saint-Gérand-Croixanvec1. 

-- insee_com22 fera la jointure avec le fichier carto
alter table cage_piketty.popcommunes add column insee_com22 varchar(5);


select '0'||codecommune as insee_com22, codecommune, nomcommune , nomdep , nomreg  from cage_piketty.popcommunes p 
where '0'||codecommune  in (select trim(c.insee_com) from cantons.communecarto2022 c);

update cage_piketty.popcommunes set insee_com22 = '0'||codecommune
from cantons.communecarto2022 c where '0'||codecommune = c.insee_com; --3143

select codecommune as insee_com22, codecommune, nomcommune , nomdep , nomreg  from cage_piketty.popcommunes p 
where codecommune in (select trim(c.insee_com) from cantons.communecarto2022 c);

update cage_piketty.popcommunes set insee_com22 = codecommune
from cantons.communecarto2022 c where codecommune = c.insee_com; -- 31683

select * from cage_piketty.popcommunes where insee_com22 is null; -- 10 (ok)
-- update manually
update cage_piketty.popcommunes set insee_com22 = '85001'  where codecommune='85307';
update cage_piketty.popcommunes set insee_com22 = '02054'  where codecommune='2695';
update cage_piketty.popcommunes set insee_com22 = '16186'  where codecommune='16010';
update cage_piketty.popcommunes set insee_com22 = '19143'  where codecommune='19092';
update cage_piketty.popcommunes set insee_com22 = '24325'  where codecommune='24089';
update cage_piketty.popcommunes set insee_com22 = '24325'  where codecommune='24314';
update cage_piketty.popcommunes set insee_com22 = '25185'  where codecommune='25134';
update cage_piketty.popcommunes set insee_com22 = '25375'  where codecommune='25628';
update cage_piketty.popcommunes set insee_com22 = '26216'  where codecommune='26219';
update cage_piketty.popcommunes set insee_com22 = '56213'  where codecommune='56049';

update cage_piketty.popcommunes c1 
set codeagglo = c2.codeagglo, nomagglo = c2.nomagglo , multicommune=c2.multicommune, numcommune=c2.numcommune
from cage_piketty.popcommunes c2
where c1.codecommune in ('85307', '2695', '16010', '19092', '24089', '24314', '25134', '25628', '26219', '56049')
and c1.insee_com22 = c2.insee_com22 and c1.codecommune != c2.codecommune;--10

update cage_piketty.popcommunes c1 
set codeagglo = c2.codeagglo, nomagglo = c2.nomagglo , multicommune=c2.multicommune, numcommune=c2.numcommune
from cage_piketty.popcommunes c2
where c1.codecommune in ('85307', '2695', '16010', '19092', '24089', '24314', '25134', '25628', '26219', '56049')
and c1.insee_com22 = c2.insee_com22 and c1.codecommune != c2.codecommune
and c2.codecommune = '24325';--2

codeagglo, nomagglo, multicommune, numcommune,

select insee_com, nom_m, statut, population  from cantons.communecarto2022 c where nom_m like '%PARIS%';
-- 75056 (Paris entière) avec 2165423 d'hab

select codecommune, nomcommune , nomdep , nomreg, paris  from cage_piketty.popcommunes p 
where p.paris = 1 or paris = 2;
delete from cage_piketty.popcommunes p  where p.paris = 1;-- 20 arrondissements

-----------------
-- jointure EPCI 

alter table cage_piketty.popcommunes add column siren_epci2022 text;
alter table cage_piketty.popcommunes add column zoneetude_epci2022 text;
alter table cage_piketty.popcommunes add column canton_cv2022 text;

update cage_piketty.popcommunes p set siren_epci2022 = c.siren_epci 
from cantons.communecarto2022 c
where c.insee_com =p.insee_com22; --34836

update cage_piketty.popcommunes p set zoneetude_epci2022 = c.zone_etude, canton_cv2022= c.cv 
from cantons.communesna22 c
where c.insee_com =p.insee_com22; --4313

-- traitement de code_agglo
select codeagglo, substring(codeagglo for 1) as commune_isolee from cage_piketty.popcommunes;

select distinct insee_com22, dep, nomdep, reg, nomreg, codeagglo, nomagglo siren_epci2022, zoneetude_epci2022
-- codeagglo, nomagglo,   
from cage_piketty.popcommunes;

select count(distinct dep) from cage_piketty.popcommunes_carto22 pc ;
-- 96


select count(distinct codecommune) from cage_piketty.popcommunes pc 
where dep = '1'; --393

select * from cage_piketty.popcommunes pc 
where pc.codecommune in ('1003', '1020', '1048', '1055', '1059', '1070', '1086', '1091', '1097',
 '1119', '1120', '1126', '1131', '1137', '1144', '1154', '1161', '1164',
 '1172', '1178', '1182', '1201', '1205', '1217', '1221', '1222', '1226',
 '1251', '1253', '1270', '1271', '1287', '1300', '1312', '1315', '1327',
 '1409', '1413', '1414', '1417', '1438', '1440', '1442', '1455', '1458');
-- aucune si dep = 1
select * from cage_piketty.popcommunes pc 
where pc.codecommune in (
'1001', '1002', '1004', '1005', '1006', '1007', '1008', '1009',
 '1010', '1011', '1447', '1448', '1449', '1450', '1451', '1452', '1453', '1454',
 '1456', '1457');
-- toutes là si dep = 01

select distinct nomdep  from cage_piketty.popcommunes pc 
where pc.dep in ('92', '93', '94', '95')

select dep, count(codecommune) 
from cage_piketty.popcommunes pc 
group by dep
order by dep;

-- select array_agg([2700, 8285, 2669, 9135, 8423, 4014, 8441, 8042, 9302, 8068, 8091], sep = ''');

select * from cage_piketty.popcommunes pc 
where pc.codecommune in (
'2700', '8285', '2669', '9135', '8423', '4014', '8441', '8042',
 '9302', '8068', '8091'); -- confirme la suppression des dep. codés en int (2, 4, 8, 9)

 select codecommune, count(distinct codecommune) as c
 from cage_piketty.naticommunes_carto22
 group by codecommune
 having count(*) > 1;

select count(distinct codecommune) from cage_piketty.naticommunes_carto22; -- 35795

select count(n.codecommune) from cage_piketty.naticommunes_carto22 n, cage_piketty.popcommunes p 
where n.codecommune = p.codecommune or n.codecommune = '0'||p.codecommune; 


select count(n.codecommune) from cage_piketty.naticommunes_carto22 n
where n.codecommune not in (select p.codecommune from cage_piketty.popcommunes p)
and n.codecommune not in (select '0'||p.codecommune from cage_piketty.popcommunes p);
-- 966
-- ce sont des communes disparues

select dep, nomdep, codecommune , nomcommune , paris, lm, pop1962, pop2018 
from cage_piketty.naticommunes_carto22 n
where n.codecommune not in (select p.codecommune from cage_piketty.popcommunes p)
and n.codecommune not in (select '0'||p.codecommune from cage_piketty.popcommunes p);

select dep, nomdep, codecommune , nomcommune , paris, lm, pop1962, pop2018 
from cage_piketty.naticommunes_carto22 n
where n.codecommune  in (select p.codecommune from cage_piketty.popcommunes p)
or n.codecommune  in (select '0'||p.codecommune from cage_piketty.popcommunes p);

select dep, nomdep, codecommune , nomcommune , paris, lm, pop1962, francais1851, pop2018 
from cage_piketty.naticommunes_carto22 n
where pop2018  > 0; -- 34830

select sum(pop1962),  sum(pop2018), sum(pop2022)
from cage_piketty.naticommunes_carto22 n
where pop2018  > 0;
--1926 1936 1946 1962 1968 1975 1982 1990 1999 2008 2013 2018 2020 2022
select sum(pop1962) as a62,  sum(pop1968) as a68, sum(pop1975) as a75, 
sum(pop1982) as a82,sum(pop1990) as a90, sum(pop1999) as a99,
sum(pop2008) as a2008, sum(pop2013) as a2013, sum(pop2018) as a2018, sum(2020) as a2020, sum(pop2022)
from cage_piketty.naticommunes_carto22 n;
--where pop2018  > 0 and petranger2020 is not null;-- j'en perd trop

-- 45246322	64864181	65765623
--- valeurs de repère
/*
1926	40 735039
1936	41 897553
1946	42 403912
1962	46 506590 - 46 580103 +
1968	49 784763 - 49 783 938 -
1975	52 648839 - 52 513326 -
1982	54 334871 - 54 295 612 -
1990	56 615155 - 56 625 026 +
1999	58 518395 - 58 520 688 +
2008	62 134866 - 62 082 041 -
2013	63 697865 - 62 852 242 -
2018	64 844037 - 64 864 181 +
2020	65 310 501 - 76 594 360 ++++ IGNORER l'année 2020 chez Piketty dans le fichier naticommunes.csv
2022	65 738100 - 65 765623 +
*/


select * from cage_piketty.naticommunes_carto22 c where c.codecommune = '75056';--Paris
select * from cage_piketty.naticommunes_carto22 c where c.codecommune = '69123';-- Lyon
select * from cage_piketty.naticommunes_carto22 c where c.codecommune = '13055';-- Marseille

-- 14650	27	EURE	27676	VENABLES
-- 34876	61	ORNE	61368	SAINT-AUBIN-DES-GROIS
/*
 * Venables est une ancienne commune française située dans le département de l'Eure, en région Normandie.

Depuis le 1er janvier 2017, elle est une commune déléguée de la commune nouvelle nommée Les Trois Lacs. 

*Saint-Aubin-des-Grois est une ancienne commune française, située dans le département de l'Orne en région Normandie, devenue le 1er janvier 2016 une commune déléguée au sein de la commune nouvelle de Perche en Nocé1. 
*
**/
alter table cage_piketty.naticommunes_carto22 add column insee_com22 text;
alter table cage_piketty.naticommunes_carto22 add column siren_epci2022 text;
alter table cage_piketty.naticommunes_carto22 add column zoneetude_epci2022 text;
alter table cage_piketty.naticommunes_carto22 add column canton_cv2022 text;

update cage_piketty.naticommunes_carto22 c 
set insee_com22 = p.insee_com22, siren_epci2022 = p.siren_epci2022, 
zoneetude_epci2022 = p.zoneetude_epci2022, canton_cv2022 = p.canton_cv2022
from cage_piketty.popcommunes p
where p.codecommune = c.codecommune or '0'||p.codecommune = c.codecommune;
-- 34836

select sum(pop1962) as a62,  sum(pop1968) as a68, sum(pop1975) as a75, 
sum(pop1982) as a82,sum(pop1990) as a90, sum(pop1999) as a99,
sum(pop2008) as a2008, sum(pop2013) as a2013, sum(pop2018) as a2018, sum(2020) as a2020, sum(pop2022)
from cage_piketty.naticommunes_carto22 n
where insee_com22 is not null;

drop table cage_piketty.naticommunes_carto22 ;
drop table cage_piketty.etrangerscommunes_carto22  ;

alter table cage_piketty.naticommunes rename to naticommunes_carto22 ;
alter table cage_piketty.etrangerscommunes rename to etrangerscommunes_carto22 ;

alter table cage_piketty.naticommunes_carto22 add column insee_com22 text;
alter table cage_piketty.naticommunes_carto22 add column siren_epci2022 text;
alter table cage_piketty.naticommunes_carto22 add column zoneetude_epci2022 text;
alter table cage_piketty.naticommunes_carto22 add column canton_cv2022 text;

alter table cage_piketty.naticommunes_carto22 add column insee_dep2022 text;
alter table cage_piketty.naticommunes_carto22 add column insee_reg2022 text;


update cage_piketty.naticommunes_carto22 c 
set insee_com22 = p.insee_com22, siren_epci2022 = p.siren_epci2022, 
zoneetude_epci2022 = p.zoneetude_epci2022, canton_cv2022 = p.canton_cv2022
from cage_piketty.popcommunes p
where p.codecommune = c.codecommune or '0'||p.codecommune = c.codecommune;
-- 34836  

update cage_piketty.naticommunes_carto22 c 
set insee_dep2022 = p.dep, insee_reg2022 = p.reg::text
from cage_piketty.popcommunes p
where p.codecommune = c.codecommune or '0'||p.codecommune = c.codecommune;

alter table cage_piketty.etrangerscommunes_carto22 add column insee_com22 text;
alter table cage_piketty.etrangerscommunes_carto22 add column siren_epci2022 text;
alter table cage_piketty.etrangerscommunes_carto22 add column zoneetude_epci2022 text;
alter table cage_piketty.etrangerscommunes_carto22 add column canton_cv2022 text;

alter table cage_piketty.etrangerscommunes_carto22 add column insee_dep2022 text;
alter table cage_piketty.etrangerscommunes_carto22 add column insee_reg2022 text;



update cage_piketty.etrangerscommunes_carto22 c 
set insee_com22 = p.insee_com22, siren_epci2022 = p.siren_epci2022, 
zoneetude_epci2022 = p.zoneetude_epci2022, canton_cv2022 = p.canton_cv2022,
insee_dep2022 = p.dep, insee_reg2022 = p.reg::text
from cage_piketty.popcommunes p
where p.codecommune = c.codecommune or '0'||p.codecommune = c.codecommune;
-- 34783 en 1 min 30 s

update cage_piketty.etrangerscommunes_carto22 c 
set insee_dep2022 = p.dep, insee_reg2022 = p.reg::text
from cage_piketty.popcommunes p
where p.codecommune = c.codecommune or '0'||p.codecommune = c.codecommune;

select * from 
cage_piketty.etrangerscommunes_carto22 e, cage_piketty.popcommunes p
where p.codecommune = e.codecommune and p.zoneetude_epci2022 is not null;

select * from 
cage_piketty.etrangerscommunes_carto22 e
where e.zoneetude_epci2022 is not null;



---

SELECT siren_epci, nom, nature, surface_km2, st_setsrid(geom, 2154) as geometry 
FROM cantons.epcina22;

select insee_dep, sum(population)
from cantons.communecarto2022 c 
 where insee_reg = '75'
group by insee_dep;

---

-- carto des séries de Piketty : SQL



select * from 
cage_piketty.etrangerscommunes_carto22 e
where e.insee_reg2022 = '75'; -- 4313 communes

select * from 
cage_piketty.etrangerscommunes_carto22 e, cantons.communesna22 c 
where e.insee_reg2022 = '75' and e.insee_com22 = c.insee_com ;
-- echelle commune

select * from (
select c.ogc_fid, c.wkb_geometry, c.insee_com, c.nom, p.pop1946, p.francais1946, p.etranger1946,  p.petranger1946  
from 
cage_piketty.etrangerscommunes_carto22 p, cantons.communesna22 c 
where p.insee_reg2022 = '75' and p.insee_com22 = c.insee_com 
) as k 
where etranger1946 is null;


select min(petranger1926), max(petranger1926), sum(etranger1926)/sum(etranger1926 + francais1926) as moyenne, stddev(petranger1926) as ecarttype, avg(petranger1926)
from cage_piketty.etrangerscommunes_carto22 e
where e.insee_reg2022 = '75'; 
-- 0.03627610235014104	0.02176200914714667710	0.02110011330296385

select stddev(petranger1936), sum(etranger1936)/sum(etranger1936 + francais1936), avg(petranger1936)
from cage_piketty.etrangerscommunes_carto22 e
where e.insee_reg2022 = '75'; 
-- 0.04335520339726	0.02655391235498475985	0.027103609909093145

select stddev(petranger1946), sum(etranger1946)/sum(etranger1946 + francais1946), avg(petranger1946)
from cage_piketty.etrangerscommunes_carto22 e
where e.insee_reg2022 = '75'; 
-- 0.04335520339726	0.02655391235498475985	0.027103609909093145
-- 0.050445383583194926	0.03003148163389513886	0.030701212475441636

select stddev(petranger1962), sum(etranger1962)/sum(etranger1962 + francais1962), avg(petranger1962)
from cage_piketty.etrangerscommunes_carto22 e
where e.insee_reg2022 = '75'; 
-- 0.03335510592167297	0.02675623415239242	0.02384613566576627

select stddev(petranger1968), sum(etranger1968)/sum(etranger1968 + francais1968), avg(petranger1968)
from cage_piketty.etrangerscommunes_carto22 e
where e.insee_reg2022 = '75'; 
-- 0.03152791063653216	0.027624634400251168	0.02165334690323281

select stddev(petranger1975), sum(etranger1975)/sum(etranger1975 + francais1975), avg(petranger1975)
from cage_piketty.etrangerscommunes_carto22 e
where e.insee_reg2022 = '75';
-- 0.029821048926257043	0.031159404764405846	0.02056447595089235

select stddev(petranger1982), sum(etranger1982)/sum(etranger1982 + francais1982), avg(petranger1982)
from cage_piketty.etrangerscommunes_carto22 e
where e.insee_reg2022 = '75';
-- 0.03498422446312515	0.03326919996862109	0.01927343774697114

select stddev(petranger1990), sum(etranger1990)/sum(etranger1990 + francais1990), avg(petranger1990)
from cage_piketty.etrangerscommunes_carto22 e
where e.insee_reg2022 = '75';
-- 0.02562394897020381	0.031674913382255816	0.01872879367113581

select stddev(petranger1999), sum(etranger1999)/sum(etranger1999 + francais1999), avg(petranger1999)
from cage_piketty.etrangerscommunes_carto22 e
where e.insee_reg2022 = '75';
-- 0.024460039148961853	0.02919932297674619	0.021574897836618694

select stddev(petranger2008), sum(etranger2008)/sum(etranger2008 + francais2008), avg(petranger2008)
from cage_piketty.etrangerscommunes_carto22 e
where e.insee_reg2022 = '75';
-- 0.035932201047054496	0.036503547536416166	0.03602090989946202

select stddev(petranger2013), sum(etranger2013)/sum(etranger2013 + francais2013), avg(petranger2013)
from cage_piketty.etrangerscommunes_carto22 e
where e.insee_reg2022 = '75';
-- 0.03984814892305404	0.04173937271617341	0.04017105423545079

select stddev(petranger2018), sum(etranger2018)/sum(etranger2018 + francais2018), avg(petranger2018)
from cage_piketty.etrangerscommunes_carto22 e
where e.insee_reg2022 = '75';
-- 0.043163732889297625	0.04800997667166368716	0.04215074151936239

select c.ogc_fid, c.wkb_geometry, c.insee_com, c.nom, p.pop1926, p.francais1926, p.etranger1926,  p.petranger1926  
from 
cage_piketty.etrangerscommunes_carto22 p, cantons.communesna22 c 
where p.insee_reg2022 = '75' and p.insee_com22 = c.insee_com ;

select c.ogc_fid, c.wkb_geometry, c.insee_com, c.nom, p.pop1936, p.francais1936, p.etranger1936,  p.petranger1936  
from 
cage_piketty.etrangerscommunes_carto22 p, cantons.communesna22 c 
where p.insee_reg2022 = '75' and p.insee_com22 = c.insee_com ;


select c.ogc_fid, c.wkb_geometry, c.insee_com, c.nom, p.pop1946, p.francais1946, p.etranger1946,  p.petranger1946  
from 
cage_piketty.etrangerscommunes_carto22 p, cantons.communesna22 c 
where p.insee_reg2022 = '75' and p.insee_com22 = c.insee_com ;

---
select c.ogc_fid, c.wkb_geometry, c.insee_com, c.nom
from 
 cantons.communesna22 c 
where c.insee_reg = '75' ;
--  p.pop1946, p.francais1946, p.etranger1946, 

select c.ogc_fid, c.wkb_geometry, c.insee_com, c.nom
from 
 cantons.communesna22 c 
where c.insee_reg = '75' ;
--  p.pop1946, p.francais1946, p.etranger1946, 

select p.insee_com22, p.etranger1946, p.etranger1962, p.etranger1968, p.etranger1975, p.etranger1982, p.etranger1990, p.etranger1999, 
p.etranger2008, p.etranger2013, p.etranger2018, p.etranger2020
from 
 cage_piketty.etrangerscommunes_carto22 p
where p.insee_reg2022 = '75' ;
--

select * from cage_piketty.etrangerscommunes_carto22
where insee_com22 = '16186';

select * from cage_piketty.etrangerscommunes_carto22
where insee_reg2022 = '75';

select * from cantons.communesna22 c 
where insee_reg = '75';

select insee_com22 
from cage_piketty.etrangerscommunes_carto22 c 
--where insee_reg2022 = '75'
group by insee_com22
having count(codecommune) > 1;
-- 24325
-- 19143
-- 16186

select insee_com22, pop1926, petranger1926 , pop2018, petranger2018 , * 
from cage_piketty.etrangerscommunes_carto22 c 
where c.insee_com22 in ('24325', '19143', '16186')
order by insee_com22, pop1926;
-- retenir : 
-- 16186, 19143, 24089 pour l'instant (pop majoritaire)
-- Liste des communes concernées (codecommune): 
/*16010
16186
19092
19143
24314
24325
24089
 * 
 */



