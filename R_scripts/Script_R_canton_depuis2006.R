###############################################################################
## Christine Plumejeaud-Perreau, U.M.R 7301 MIGRINTER
## 07 octobre 2020
## Script d'analyse des données INSEE pour IMHANA
## Mise à jour 21 sept 2023
###############################################################################

meslibrairiesR <- "C:/Tools/R/R4"
# Ajouter meslibrairiesR dans ces chemins : 
.libPaths(c( .libPaths(), meslibrairiesR) )


install.packages("tidyverse", meslibrairiesR)
install.packages("mapsf", meslibrairiesR)
install.packages("dplyr", meslibrairiesR)

library(tidyverse)
library(mapsf)
library(dplyr)  
library(foreign)

setwd("C:/Travail/MIGRINTER/Labo/IMHANA/data/Data_canton-ville/")
getwd() 

## Sauver / reprendre son travail sous R
# https://mtes-mct.github.io/parcours-r/m1/sauvegarder-son-travail.html 
dir.create("./outputs")

save(list = ls(), file = "outputs/env_entier.RData") 
# sauvegarde de tout l'environnement sur le répertoire choisi 

rm(list = ls()) # suppression de notre environnement dans R 

load("outputs/env_entier.RData") 
# chargement de l'environnement stocké sur l'ordinateur 


######################################################################### programme de lecture du fichier détail de l'Enquête emploi 2019_fichier CSV.R

#https://www.insee.fr/fr/statistiques/fichier/2011208/rp2011_indcvizc_dbase.zip

require(httr)
require(curl)



targetURL <- 'https://www.insee.fr/fr/statistiques/fichier/2011208/rp2011_indcvizc_dbase.zip'

#zipfile <- httr::GET(url = 'https://www.insee.fr/fr/statistiques/fichier/2011208/rp2011_indcvizc_dbase.zip')
#tmp <- tempfile()
#C:\\Users\\cplumeje\\AppData\\Local\\Temp\\RtmpwlITvr\\file7105ea72378"
#curl_download(targetURL, './ouputs/rp2011_indcvizc_dbase.zip')
#curl_download(targetURL, tmp)



#un fichier DBF à dezipper depuis le site INSEE
temp <- tempfile()
#telecharger le zip depuis internet, sauvegarder dans temp
download.file(targetURL,temp, mode="wb")
#Ca bugue à cause du fait que ca prend plus de 60 seconds 
#URL 'https://www.insee.fr/fr/statistiques/fichier/2011208/rp2011_indcvizc_dbase.zip': Timeout of 60 seconds was reached
#downloaded 61.1 MB

#Dezipper le fichier temps et enregistrer les resources déziper dans temp2
temp2 <- tempfile()
unzip(zipfile = 'rp2011_indcvizc_dbase.zip', exdir = temp2)

#recuperer le chemin d'accès du the shapefile (.shp) file dans le fichieer deziper temp2
#le $ à la fin de  ".shp$" assure de ne pas trouver des fichier du style .shp.xml
data_insee_dbf<-list.files(temp2, pattern = ".dbf$",full.names=TRUE)

print(data_insee_dbf)
#"C:\\Users\\cplumeje\\AppData\\Local\\Temp\\RtmpwlITvr\\file7104aa24389/FD_indcvizc_2011.dbf"
system.file(data_insee_dbf, package="foreign")[1]

x <- read.dbf(data_insee_dbf) #4089201 obs de 89 var

unique(x$CANTVILLE)
#5401 5402 5403 5404 5405 5406 5407 5408 5409 5410 5411 5412 5413 5414 5415 5416 5417 5418 5423
[441] 5424 5425 5426 5427 5428 5429 5430 5431 5432 5433 5434 5435 5437 5438 5439 5440 5441 5442 5443 5496 5497 5498
[463] 5499

unique(x$METRODOM)

str(x)
summary(x)

############################### lecture 2020

#C:\Travail\MIGRINTER\Labo\IMHANA\data\Data_canton-ville\2020
#  https://www.insee.fr/fr/statistiques/7637890
# Logements, individus, activité, mobilités scolaires et professionnelles, migrations résidentielles en 2020 
# Recensement de la population - Fichier détail
# https://www.insee.fr/fr/statistiques/7706119?sommaire=7637890

# données localisées au canton-ou-ville (et à l'IRIS, pour les IRIS d'au moins 200 habitants) décrivant les caractéristiques de chaque personne recensée, celles de son ménage, ainsi que celles de sa résidence principale,
# variables à modalités agrégées,
# variables de tri : canton-ou-ville et numéro du ménage dans le canton-ou-ville , https://www.insee.fr/fr/statistiques/fichier/7706119/dictionnaire_indcvi.pdf
# champ : tous individus.

data = read.csv2("2020/FD_INDCVIZD_2020.csv", header = TRUE, sep = ";", quote = "\"",
                 dec = ".", fill = TRUE, comment.char = "")	

library(tidyverse)
library(lubridate)
data <- data %>%
  mutate(ANAI_date = as.Date(as.character(ANAI), format = "%Y")) %>% 
  mutate(ANAI_int = as.numeric(ANAI)) %>% 
  mutate_at(vars(CANTVILLE, ANAI, APAF, CS1, DIPL, DNAI, EMPL, ETUD, ILT, IMMI, INAI, INATC, IRAN, LIENF, LPRF, LPRM,METRODOM, NAIDT, ORIDT, SEXE, SFM ), list(as.factor))  %>%
  mutate_at(vars(INPERF), list(as.numeric))

## 03 nov : résumé par CV des données pour Francesca


resume <- data %>% filter ( REGION %in% c(75) & IMMI %in% c(1) ) %>%
  group_by(CANTVILLE, ANAI_int, APAF, CS1, DIPL, DNAI, EMPL, ETUD, ILT, IMMI, INAI, INATC, IRAN, LIENF, LPRF, LPRM,METRODOM, NAIDT, ORIDT, SEXE, SFM) %>%
  summarise(total = round(sum(IPONDI))
  ) %>%
  arrange(CANTVILLE,ANAI_int)
write.table(resume, "./outputs/cpp_resume_FD_INDCVIZD_2020_region75_IMMI2.csv", sep = ";", dec=".", row.names=FALSE, fileEncoding = "UTF-8")

#Les français
resume <- data %>% filter ( REGION %in% c(75) & IMMI %in% c(2) ) %>%
  group_by(CANTVILLE, ANAI_int, APAF, CS1, DIPL, DNAI, EMPL, ETUD, ILT, INAI, IRAN, LIENF, LPRF, LPRM,METRODOM, NAIDT, ORIDT, SEXE, SFM) %>%
  summarise(total = round(sum(IPONDI))
  ) %>%
  arrange(CANTVILLE,ANAI_int)
write.table(resume, "./outputs/cpp_resume_FD_INDCVIZD_2020_region75_IMMI2.csv", sep = ";", dec=".", row.names=FALSE, fileEncoding = "UTF-8")
#Dans la region 75 ca n'a pas marché

resume$DEPARTEMENT = substr(resume$CANTVILLE, 0, 2)
resume <- resume%>%
  mutate_at(vars(DEPARTEMENT), list(as.factor))  %>%
  filter ( DEPARTEMENT %in% c(79)  )     

#Export francais limité au département 79
write.table(resume, "./outputs/cpp_resume_FD_INDCVIZD_2020_DEPART79_IMMI2.csv", sep = ";", dec=".", row.names=FALSE, fileEncoding = "UTF-8")


## 25 jan : résumé fr/etr pour la région 75

resume <- data %>% filter ( REGION %in% c(75)  ) %>%
  group_by(CANTVILLE, ANAI_int, APAF, CS1, DIPL, DNAI, EMPL, ETUD, ILT, IMMI, INAI, INATC, IRAN, LIENF, LPRF, LPRM,METRODOM, NAIDT, ORIDT, SEXE, SFM) %>%
  summarise(total = round(sum(IPONDI))
  ) %>%
  arrange(CANTVILLE,ANAI_int)
write.table(resume, "./outputs/cpp_resume_FD_INDCVIZD_2020_region75_tout.csv", sep = ";", dec=".", row.names=FALSE, fileEncoding = "UTF-8")
resume$DEPARTEMENT = substr(resume$CANTVILLE, 0, 2)
